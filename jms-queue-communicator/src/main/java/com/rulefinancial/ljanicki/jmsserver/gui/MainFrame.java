package com.rulefinancial.ljanicki.jmsserver.gui;

import java.awt.Dimension;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.rulefinancial.ljanicki.jmsserver.Communication;

/**
 * @author <a href="mailto:Lukasz.Janicki@rulefinancial.com">Lukasz Janicki</a>
 * 
 */
public class MainFrame extends JFrame {

	private Communication communication;

	private TextArea chatArea;

	private JTextField me;

	public MainFrame() {
		super("Chat");
		setResizable(false);
		setSize(new Dimension(400, 350));
		setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		chooseParticipants();
	}

	/**
	 * 
	 */
	private void chooseParticipants() {
		add(new JLabel("your nick:"));
		me = new JTextField();
		me.setToolTipText("your name");
		add(me);

		add(new JLabel("who do you want to talk to:"));
		final JTextField participant = new JTextField();
		participant.setToolTipText("talk to");
		add(participant);

		JButton startTalk = new JButton("start conversation");
		startTalk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				communication = new Communication();
				communication.establishConnection(me.getText(),
						participant.getText());
				communication.setMessageHandler(new MessageListener() {
					public void onMessage(Message message) {
						if (message instanceof TextMessage) {
							try {
								chatArea.append("(" + participant.getText()
										+ ") ");
								chatArea.append(((TextMessage) message)
										.getText());
								chatArea.append("\n");
							} catch (JMSException e) {
								e.printStackTrace();
							}
						}
					}
				});
				setTitle("Chat with " + participant.getText());
				getContentPane().removeAll();
				showChatWindow();
				repaint();
				revalidate();
			}
		});
		add(startTalk);

	}

	/**
	 */
	private void showChatWindow() {
		add(new JLabel("conversation:"));
		chatArea = new TextArea();
		chatArea.setEditable(false);
		add(chatArea);

		add(new JLabel("your message:"));
		final JTextField message = new JTextField();
		add(message);

		JButton sendButton = new JButton("Send");
		sendButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sentTxt = message.getText();
				message.setText("");
				communication.sendMessage(sentTxt);
				chatArea.append("(" + me.getText() + ") ");
				chatArea.append(sentTxt);
				chatArea.append("\n");
			}
		});
		add(sendButton);
	}

}
