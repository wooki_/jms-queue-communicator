package com.rulefinancial.ljanicki.jmsserver;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 * @author <a href="mailto:Lukasz.Janicki@rulefinancial.com">Lukasz Janicki</a>
 */
public class Communication {

	private MessageProducer producer;
	private MessageConsumer consumer;
	private Session session;

	public void establishConnection(String me, String participant) {
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				ActiveMQConnection.DEFAULT_BROKER_URL);
		try {
			Connection connection = connectionFactory.createConnection();
			connection.start();
			session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Queue consumerQueue = session.createQueue(me);
			Queue producerQueue = session.createQueue(participant);
			consumer = session.createConsumer(consumerQueue);
			producer = session.createProducer(producerQueue);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(String sentTxt) {
		try {
			producer.send(session.createTextMessage(sentTxt));
		} catch (JMSException e) {
			System.out.println(e.getMessage());
		}
	}

	/**
	 * @param listener 
	 * 
	 */
	public void setMessageHandler(MessageListener listener) {
		try {
			consumer.setMessageListener(listener);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
