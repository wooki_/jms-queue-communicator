package com.rulefinancial.ljanicki.jmsserver;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import com.rulefinancial.ljanicki.jmsserver.gui.MainFrame;

/**
 * @author <a href="mailto:Lukasz.Janicki@rulefinancial.com">Lukasz Janicki</a>
 */
public class Main {

	public static void main(String[] args) {
		
		MainFrame main = new MainFrame();
		main.setVisible(true);
/*
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
				ActiveMQConnection.DEFAULT_BROKER_URL);
		try {
			Connection connection = connectionFactory.createConnection();
			connection.start();
			Session session = connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			Queue queue = session.createQueue("test_fruits");
			MessageConsumer consumer = session.createConsumer(queue);
			consumer.setMessageListener(new MessageListener() {
				public void onMessage(Message arg0) {
					try {
						if (arg0 instanceof TextMessage) {
							System.out.println(((TextMessage) arg0).getText());
						} else {
							System.out.println(arg0);
						}
					} catch (JMSException e) {
						e.printStackTrace();
					}
				}
			});
			while (true) {

			}
			//connection.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}
		*/

	}

}
